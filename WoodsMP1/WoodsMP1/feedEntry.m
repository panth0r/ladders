//
//  ParsedInput.m
//  WoodsMP1
//
//  Created by Michael Kory Woods on 2/22/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

#import "feedEntry.h"

@implementation feedEntry
@synthesize title, description;
//- (id)init {
//  return self;
//}
- (id)initWithTitle:(NSString*)aTitle andDescription:(NSString*)aStory {
  title = aTitle;
  description = aStory;
  return self;
}
- (NSString*)getTitle {
  return title;
}
- (NSString*)getDescriptionOfLength:(NSUInteger)length {
  if ([description length] >= length){
    return [description substringToIndex:length];
  } else {
    return description;
  }
}
@end