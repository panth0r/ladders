//
//  main.m
//  WoodsMP1
//
//  Created by Michael Kory Woods on 2/18/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//
//  WHY AM I FIGHTING THE TYPE SYSTEM SO MUCH!?!??!?!?
//    Maybe I'm not fighting it as much as I think I am,
//    just recently with NSNumber, NSUInteger and similar typedefs.
//  Besides that, so far I've implemented the assignment
//    plus the first two "suggested extras."
//  On failing to parse (ie, if [parser parse] or whatever the call)
//    returns false or NO or whatnot, a generic message is printed.
//  Oh, and I think this (specifically the FeedEntry class)
//    is probably pretty poor Objective-C;
//    mostly due to any number of nuances of the language.
//    Not to blame the language, that is;
//      can't imagine too many virtuosos writing idiomatic code out the gate.

#import <Foundation/Foundation.h>
#import "delegate.h"
#import "feedEntry.h"

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    NSArray *feeds = [NSArray arrayWithContentsOfFile:@"settings.plist"];
    [feeds enumerateObjectsUsingBlock:^(id feed, NSUInteger i, BOOL *stop) {
      if(i >= 1) printf("\n");
      NSURL *feedURL = [NSURL URLWithString:feed[@"FeedURL"]];
      NSNumber *maxItems = feed[@"MaxItems"];
      NSNumber *maxLength = feed[@"StoryLength"];
      NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:feedURL];
      delegate *d0 = [[delegate alloc] init];
      [parser setDelegate:d0];
      printf("Feed %ld: %s\n", (i+1), [[feedURL description] cStringUsingEncoding:NSUTF8StringEncoding]);
      if([parser parse]) {
        printf("%s\n", "Parsing enabled and completed");
        printf("Title of Feed: %s \n", [[d0 getTitleOfFeed] cStringUsingEncoding:NSUTF8StringEncoding]);
        NSMutableArray* parsedFeeds = [d0 getFeeds];
        printf("Found %ld stories; limiting to the most recent %d:\n", [parsedFeeds count], [maxItems intValue]);
        [parsedFeeds enumerateObjectsUsingBlock:^(id entry, NSUInteger j, BOOL *stop) {
          printf("Title: %s\n", [[entry getTitle] cStringUsingEncoding:NSUTF8StringEncoding]);
          printf("Excerpt: %s\n", [[entry getDescriptionOfLength:[maxLength unsignedIntegerValue]] cStringUsingEncoding:NSUTF8StringEncoding]);
          if ( j >= ([maxItems intValue]-1) ) *stop = YES;
        }];
      } else printf("parsing of feed failed\n");
    }];
  }
  return 0;
}