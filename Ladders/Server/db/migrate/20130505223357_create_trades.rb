class CreateTrades < ActiveRecord::Migration
  def change
    create_table :trades do |t|
      t.integer :entered_price
      t.integer :filled_price
      t.boolean :longTrue_shortFalse
      t.boolean :is_executed
      t.datetime :executed_at
      t.boolean :is_filled
      t.datetime :filled_at
      t.string :user
      t.string :product_name
      t.integer :trade_queue_id

      t.timestamps
    end
  end
end
