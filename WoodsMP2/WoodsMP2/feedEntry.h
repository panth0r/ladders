//
//  ParsedInput.h
//  WoodsMP1
//
//  Created by Michael Kory Woods on 2/22/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface feedEntry : NSObject
@property NSString *title;
@property NSString *description;
//- (id)init;
- (id)initWithTitle:(NSString*)aTitle andDescription:(NSString*)aStory;
- (NSString*)getTitle;
- (NSString*)getDescriptionOfLength:(NSUInteger)length;
@end