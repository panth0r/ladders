//
//  mp2reduxCell.h
//  WoodsMP2
//
//  Created by Michael Kory Woods on 3/17/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mp2reduxCell : UITableViewCell
@property (nonatomic) IBOutlet UILabel* titleLable;
@property (nonatomic) IBOutlet UILabel* dateLabel;
@property (nonatomic) IBOutlet UILabel* descriptionLabel;
@end
