//
//  LCMasterViewController.m
//  Client
//
//  Created by Michael Kory Woods on 5/15/13.
//  Copyright (c) 2013 Woods. All rights reserved.
//

#import "LCMasterViewController.h"
#import "LCDetailViewController.h"
#import "LCClient.h"
#import "AFJSONRequestOperation.h"

@interface LCMasterViewController () {
  NSMutableArray *_objects;
}
@property(strong) NSArray *trades;
@end

@implementation LCMasterViewController

- (void)awakeFromNib
{
//  NSURL *url = [NSURL URLWithString:@"http://192.168.1.64:3000/trades.json"];
//  NSURLRequest *request = [NSURLRequest requestWithURL:url];
//  AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
//    NSLog(@"Username: %@", [JSON valueForKeyPath:@"user"]);
//    NSLog(@"%@", [JSON class]);
//    NSLog(@"%d, %@",[JSON count], [JSON[25] class]);
//    NSLog(@"1) %@, %d", [JSON class], [JSON count]);
//    self.trades = JSON;
//    NSLog(@"0) %@, %d", self.trades.class, self.trades.count);
//  } failure:nil];
//  [operation start];
  // Override point for customization after application launch.
  self.clearsSelectionOnViewWillAppear = NO;
  self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
  NSLog(@"2) %lu", (unsigned long)self.trades.count);
  [super awakeFromNib];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  NSLog(@"3) %d",[self.trades count]);
  
	// Do any additional setup after loading the view, typically from a nib.
  self.navigationItem.leftBarButtonItem = self.editButtonItem;
  
  UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
  self.navigationItem.rightBarButtonItem = addButton;
  self.detailViewController = (LCDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
  
  self.trades = [[NSArray alloc] init];
  
  NSURL *url = [[NSURL alloc] initWithString:@"http://localhost:3000/trades.json"];
  NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
  
  AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
    NSLog(@"%@, here0", JSON);
    self.trades = [JSON objectForKey:@"id"];
//    [self.activityIndicatorView stopAnimating];
    [self.tableView setHidden:NO];
    [self.tableView reloadData];
    
  } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
    NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
  }];
  
  [operation start];

}

- (void)didReceiveMemoryWarning
{
  
  NSLog(@"4) %d",[self.trades count]);
  NSLog(@"5) %@", self.trades.class);
  
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
  if (!_objects) {
    _objects = [[NSMutableArray alloc] init];
  }
  [_objects insertObject:[NSDate date] atIndex:0];
  NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
  [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//  return self.trades.count;
  if (self.trades && self.trades.count) {
    return self.trades.count;
  } else {
    return 0;
  }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
  
//  NSLog(@"7) %@", self.trades[indexPath]);
//  cell.textLabel.text = self.trades[indexPath];

//  NSDate *object = _objects[indexPath.row];
//  cell.textLabel.text = [object description];
//  return cell;
  
//  static NSString *cellID = @"Cell";
//  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
//  if (!cell) {
//    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
//  }
//  return cell;
  static NSString *cellID = @"Cell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
  
//  if (!cell) {
//    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
//  }
  
  NSDictionary *trade = [self.trades objectAtIndex:indexPath.row];
  cell.textLabel.text = [trade objectForKey:@"product_name"];
//  cell.detailTextLabel.text = [trade objectForKey:@"entered_price"];
  
//  NSURL *url = [[NSURL alloc] initWithString:[movie objectForKey:@"artworkUrl100"]];
//  [cell.imageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder"]];
  
  return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
  // Return NO if you do not want the specified item to be editable.
  return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    [_objects removeObjectAtIndex:indexPath.row];
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
  } else if (editingStyle == UITableViewCellEditingStyleInsert) {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
  }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSDate *object = _objects[indexPath.row];
  self.detailViewController.detailItem = object;
}

@end