//
//  LCClient.h
//  Client
//
//  Created by Michael Kory Woods on 5/15/13.
//  Copyright (c) 2013 Woods. All rights reserved.
//

#import "AFHTTPClient.h"

@interface LCClient : AFHTTPClient
+(LCClient *)sharedClient;
@end
