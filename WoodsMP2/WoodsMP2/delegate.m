//
//  delegate
//  WoodsMP1
//
//  Created by Michael Kory Woods on 2/18/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

//  gurgitate the xml file and vomit it back up

#import "delegate.h"
#import "feedEntry.h"
@implementation delegate {
  NSMutableArray *feeds;
  bool inItemOrEntry;
  bool inTitle;
  bool inDescription;
  int titleNumber;
  NSMutableString *title;
  NSString *titleOfFeed;
  NSMutableString *description;
}
- (id)init {
  feeds = [[NSMutableArray alloc] init];
  titleNumber = 0;
  inItemOrEntry = false;
  inTitle = false;
  inDescription = false;
  title = [[NSMutableString alloc] init];
  description = [[NSMutableString alloc] init];
  return self;
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
  if([elementName isEqualToString:@"item"] || [elementName isEqualToString:@"entry"]) inItemOrEntry = true;
  if([elementName isEqualToString:@"title"]) inTitle = true;
  if([elementName isEqualToString:@"description"] || [elementName isEqualToString:@"content"]) inDescription = true;
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
  if([elementName isEqualToString:@"title"]) inTitle = false;
  if([elementName isEqualToString:@"description"] || [elementName isEqualToString:@"content"]) inDescription = false;
  if([elementName isEqualToString:@"item"] || [elementName isEqualToString:@"entry"]) {
    inItemOrEntry = false;
    [feeds addObject:[[feedEntry alloc] initWithTitle:[NSString stringWithString:title] andDescription:[NSString stringWithString:description]]];
    [title setString:@""];
    [description setString:@""];
    titleNumber++;
  }
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
  if (inItemOrEntry) {
    if(inTitle) {
      [title appendString:[string stringByReplacingOccurrencesOfString:@"\n" withString:@" "]];
    }
    if(inDescription) {
      [description appendString:[string stringByReplacingOccurrencesOfString:@"\n" withString:@" "]];
    }
  } else if (inTitle) {
    titleOfFeed = [NSString stringWithString:string];
  }
}
- (NSMutableArray*)getFeeds {
  return feeds;
}
- (NSString*)getTitleOfFeed {
  return titleOfFeed;
}
@end