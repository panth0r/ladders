//
//  main.m
//  Client
//
//  Created by Michael Kory Woods on 5/15/13.
//  Copyright (c) 2013 Woods. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LCAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([LCAppDelegate class]));
  }
}
