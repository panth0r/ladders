//  LCClient.m
//  Client
//  Created by Michael Kory Woods on 5/15/13.
//  Copyright (c) 2013 Woods. All rights reserved.

#import "LCClient.h"
#import "AFJSONRequestOperation.h"

static NSString * const baseURL = @"http://192.168.1.64:3000/";

@implementation LCClient

+(LCClient *)sharedClient {
  static LCClient *_sharedClient = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _sharedClient = [[LCClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
  });
  return _sharedClient;
}

-(id)initWithBaseURL:(NSURL *)url {
  self = [super initWithBaseURL:url];
  if (!self) { return nil; }
  [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
	[self setDefaultHeader:@"Accept" value:@"application/json"];
  return self;
}

@end