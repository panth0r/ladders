# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

TradeQueue.create({
  product_name: "ES",
  is_active: true
})

#valid market state: (4*longs@(343, 344), 4*shorts@(345, 346))
Trade.create({
  entered_price: 343,
  longTrue_shortFalse: true,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 343,
  longTrue_shortFalse: true,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 343,
  longTrue_shortFalse: true,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 343,
  longTrue_shortFalse: true,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 344,
  longTrue_shortFalse: true,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 344,
  longTrue_shortFalse: true,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 344,
  longTrue_shortFalse: true,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 344,
  longTrue_shortFalse: true,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 345,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 345,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 345,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 345,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 346,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 346,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 346,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 346,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

#buy order above the spread, executed immediately
Trade.create({
  entered_price: 345,
  longTrue_shortFalse: true,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

#sell order below the spread, likewise
Trade.create({
  entered_price: 344,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

#three offers to shift the market down
Trade.create({
  entered_price: 344,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 344,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 344,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

#22 #and a forth closes the spread (executed not filled)
Trade.create({
  entered_price: 344,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "panth0r",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 344,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "kory",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 344,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "kory",
  product_name: "ES",
  trade_queue_id: 1
})

Trade.create({
  entered_price: 344,
  longTrue_shortFalse: false,
  is_executed: false,
  executed_at: nil,
  is_filled: false,
  filled_at: nil,
  user: "kory",
  product_name: "ES",
  trade_queue_id: 1
})