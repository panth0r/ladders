//
//  mp2reduxCell.m
//  WoodsMP2
//
//  Created by Michael Kory Woods on 3/17/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

#import "mp2reduxCell.h"

@implementation mp2reduxCell
@synthesize titleLable, descriptionLabel, dateLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
