//
//  mp2reduxViewController.m
//  mp2redux
//
//  Created by Michael Kory Woods on 3/10/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//
/*
 Gah, good case of shoulda started it earlier, going to branch it and try
    to fix what's not working when back on a Mac; spent too much time stupidly
    looking into "NSIndexPath," "NSIndexPath UIKit Additions" and "UITableView"
    that should have been spent more specifically reviewing the method
    "tableView:cellForRowAtIndexPath."  After a couple samples, it's starting
    to make more sense.
 */

#import "WoodsMP2ViewController.h"
#import "delegate.h"
#import "feedEntry.h"

@interface mp2reduxViewController () {
  NSMutableArray *feedTitlesOrSections;
  NSMutableArray *feedsInSection;
}
@property (weak, nonatomic) IBOutlet UILabel *Label;
@end
@implementation mp2reduxViewController
- (void)viewDidLoad {
  [super viewDidLoad];
  feedTitlesOrSections = [[NSMutableArray alloc] init];
  feedsInSection = [[NSMutableArray alloc] init];
}
- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}
- (IBAction)fetchStories:(id)sender {
  delegate    *d = [[delegate alloc] init];
  NSXMLParser *p = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:self.feedURLField.text]];
  [p setDelegate:d];
  if([p parse]) {
    [feedTitlesOrSections addObject:[d getTitleOfFeed]];
    [feedsInSection addObject:[NSArray arrayWithArray:[d getFeeds]]];
  }
  [self.tableView reloadData];
  [self.feedURLField resignFirstResponder];
}
- (IBAction)numberOfStoriesSlider:(id)sender {
  [self.tableView reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return feedTitlesOrSections.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"StoryCell"];
  //HAAAAAAAAAA: if you can think of worse name than "description" for an
  //  instance variable, I'd love to hear it; it might make me feel better
  cell.textLabel.text = [[[feedsInSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] title];
  return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
  NSMutableString *sectionTitle = feedTitlesOrSections[section];
  return sectionTitle;
}
@end
