//
//  mp2AppDelegate.h
//  WoodsMP2
//
//  Created by Michael Kory Woods on 3/10/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mp2AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
