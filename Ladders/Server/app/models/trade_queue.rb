# an extremely simplified exchange, maintains a queue of trades;
# when an order is entered,
#  if it's a sell below the bid or a buy above the ask (ie, a market order)
#   match with the oldest instance of the highest bid or lowest ask,
#   respectively
#  otherwise it is queued at the desired price

class TradeQueue < ActiveRecord::Base
  attr_accessible :is_active, :product_name, :high_bid, :low_offer
  attr_accessor :high_bid, :low_offer
  has_many :trades
#  belongs_to :product
  def queue_trade aTrade
    if aTrade.longTrue_shortFalse == true
      et = trades.where([
        "entered_price <= ? AND longTrue_shortFalse == ? AND is_filled == ?",
        aTrade.entered_price, false, false
      ]).first
    elsif aTrade.longTrue_shortFalse == false
      et = trades.where([
        "entered_price >= ? AND longTrue_shortFalse == ? AND is_filled == ?",
        aTrade.entered_price, true, false
      ]).first
    else #invalid; trade neither long nor short
    end
    unless et.nil?
      et.assign_attributes ({:is_filled => true, :filled_at => DateTime.now})
      aTrade.assign_attributes ({:is_filled => true, :filled_at => DateTime.now})
      et.save
      aTrade.save
    end
  end
  def match_trades sTrade, bTrade
    
  end
  def update_high_bid
    price = trades.where( :is_filled => false,
                          :longTrue_shortFalse => true).maximum("entered_price")
    @high_bid = trades.where( :is_filled => false,
                              :longTrue_shortFalse => true,
                              :entered_price => price).first
  end
  def update_low_offer
    price = trades.where( :is_filled => false,
                          :longTrue_shortFalse => false).minimum("entered_price")
    @low_offer = trades.where(  :is_filled => false,
                                :longTrue_shortFalse => false,
                                :entered_price => price).first
  end
end