class CreateTradeQueues < ActiveRecord::Migration
  def change
    create_table :trade_queues do |t|
      t.boolean :is_active
      t.string :product_name

      t.timestamps
    end
  end
end
