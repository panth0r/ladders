//
//  main.m
//  mp2redux
//
//  Created by Michael Kory Woods on 3/10/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WoodsMP2.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([WoodsMP2 class]));
  }
}
