# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130505224530) do

  create_table "trade_queues", :force => true do |t|
    t.boolean  "is_active"
    t.string   "product_name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "trades", :force => true do |t|
    t.integer  "entered_price"
    t.integer  "filled_price"
    t.boolean  "longTrue_shortFalse"
    t.boolean  "is_executed"
    t.datetime "executed_at"
    t.boolean  "is_filled"
    t.datetime "filled_at"
    t.string   "user"
    t.string   "product_name"
    t.integer  "trade_queue_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

end
