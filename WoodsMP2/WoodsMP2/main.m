//
//  main.m
//  WoodsMP2
//
//  Created by Michael Kory Woods on 3/10/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "mp2AppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([mp2AppDelegate class]));
  }
}
