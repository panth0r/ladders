//
//  delegate
//  WoodsMP1
//
//  Created by Michael Kory Woods on 2/18/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface delegate : NSObject <NSXMLParserDelegate>
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict;
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName;
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string;
- (NSMutableArray*)getFeeds;
- (NSString*)getTitleOfFeed;
@end
