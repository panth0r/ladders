//
//  LCMasterViewController.h
//  Client
//
//  Created by Michael Kory Woods on 5/15/13.
//  Copyright (c) 2013 Woods. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LCDetailViewController;

@interface LCMasterViewController : UITableViewController

@property (strong, nonatomic) LCDetailViewController *detailViewController;

@end
