//
//  LCAppDelegate.h
//  Client
//
//  Created by Michael Kory Woods on 5/15/13.
//  Copyright (c) 2013 Woods. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
