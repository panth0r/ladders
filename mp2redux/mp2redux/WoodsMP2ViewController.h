//
//  mp2reduxViewController.h
//  mp2redux
//
//  Created by Michael Kory Woods on 3/10/13.
//  Copyright (c) 2013 Michael Kory Woods. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mp2reduxViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *feedURLField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)fetchStories:(id)sender;
@end
