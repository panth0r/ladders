class Trade < ActiveRecord::Base
  attr_accessible :id,
                  :executed_at,
                  :filled_at,
                  :is_executed,
                  :is_filled,
                  :longTrue_shortFalse,
                  :entered_price,
                  :filled_price,
                  :product_name,
                  :user,
                  :trade_queue_id
  belongs_to  :trade_queue
  before_save :try_execute

private  
  def try_execute
    self.assign_attributes({:is_executed => true, :executed_at => DateTime.now})
    if(is_filled == false)
      trade_queue.queue_trade self
    end
  end
end